package top.yueyazhui.learn_shiro_spring_boot.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author yueyazhui
 * @Date 2023/2/18
 */
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Response {

    private Integer code;
    private String message;
    private Object data;

    public static Response success(String message) {
        return new Response(200, message, null);
    }

    public static Response success(String message, Object data) {
        return new Response(200, message, data);
    }

    public static Response error(String message) {
        return new Response(500, message, null);
    }

    public static Response error(String message, Object data) {
        return new Response(500, message, data);
    }
}
