package top.yueyazhui.learn_shiro_spring_boot.config;

import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.yueyazhui.learn_shiro_spring_boot.realm.MyRealm02;

/**
 * @author yueyazhui
 * @Date 2023-03-19
 */
@Configuration
public class ShiroConfig {

    @Autowired
    MyRealm02 myRealm02;

    @Bean
    DefaultWebSecurityManager defaultWebSecurityManager () {
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        defaultWebSecurityManager.setRealm(myRealm02);
        return defaultWebSecurityManager;
    }

    @Bean
    ShiroFilterChainDefinition shiroFilterChainDefinition () {
        DefaultShiroFilterChainDefinition shiroFilterChainDefinition = new DefaultShiroFilterChainDefinition();
        shiroFilterChainDefinition.addPathDefinition("/login", "anon");
        shiroFilterChainDefinition.addPathDefinition("/logout", "logout");
        shiroFilterChainDefinition.addPathDefinition("/**", "authc");
        return shiroFilterChainDefinition;
    }
}
