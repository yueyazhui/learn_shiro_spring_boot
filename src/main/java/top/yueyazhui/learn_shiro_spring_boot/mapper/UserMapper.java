package top.yueyazhui.learn_shiro_spring_boot.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.yueyazhui.learn_shiro_spring_boot.entity.User;

import java.util.Set;

@Mapper
public interface UserMapper {

    User getUserByUsername(String username);

    Set<String> findRolesByUsername(String username);
}
