package top.yueyazhui.learn_shiro_spring_boot.controller;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author yueyazhui
 * @Date 2023/2/18
 */
@RestController
public class HelloController {

    @GetMapping("hello")
    public String hello() {
        return "Hello";
    }

    @GetMapping("admin")
    @RequiresRoles("admin")
    public String admin() {
        return "Hello Admin";
    }
}
