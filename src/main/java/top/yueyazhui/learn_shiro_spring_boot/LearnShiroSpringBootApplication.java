package top.yueyazhui.learn_shiro_spring_boot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "top.yueyazhui.learn_shiro_spring_boot.mapper")
public class LearnShiroSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearnShiroSpringBootApplication.class, args);
    }

}
