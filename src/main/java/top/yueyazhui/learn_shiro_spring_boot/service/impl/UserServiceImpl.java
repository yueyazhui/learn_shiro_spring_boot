package top.yueyazhui.learn_shiro_spring_boot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.yueyazhui.learn_shiro_spring_boot.entity.User;
import top.yueyazhui.learn_shiro_spring_boot.mapper.UserMapper;
import top.yueyazhui.learn_shiro_spring_boot.service.UserService;

import java.util.Set;

/**
 * @Author yueyazhui
 * @Date 2023/2/18
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    /**
     * @param username
     * @return
     */
    @Override
    public User getUserByUsername(String username) {
        return userMapper.getUserByUsername(username);
    }

    /**
     * @param username
     * @return
     */
    @Override
    public Set<String> findRolesByUsername(String username) {
        return userMapper.findRolesByUsername(username);
    }
}
