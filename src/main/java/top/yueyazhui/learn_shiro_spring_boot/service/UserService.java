package top.yueyazhui.learn_shiro_spring_boot.service;

import top.yueyazhui.learn_shiro_spring_boot.entity.User;

import java.util.Set;

public interface UserService {

    User getUserByUsername(String username);

    Set<String> findRolesByUsername(String username);
}
