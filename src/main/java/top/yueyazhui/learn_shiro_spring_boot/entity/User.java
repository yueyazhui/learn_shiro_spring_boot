package top.yueyazhui.learn_shiro_spring_boot.entity;

import lombok.Data;

/**
 * @Author yueyazhui
 * @Date 2023/2/18
 */
@Data
public class User {

    private Integer id;
    private String username;
    private String password;
}
